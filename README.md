# Demo Analysis Tool for CounterStrike Global Offensive
This is a Demo Analysis Tool specialized on finding data relevant to pro teams in order to gain new insights more efficiently.

## Status
Currently, this tool is in concept phase. We are trying to determine if this kind of automated analysis is worth the effort. This is not intended to be used yet.
