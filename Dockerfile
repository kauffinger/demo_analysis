FROM python:3.10.5-slim-bullseye

RUN apt update && apt upgrade -y
RUN apt install wget software-properties-common apt-transport-https bash -y
RUN wget https://golang.org/dl/go1.17.linux-amd64.tar.gz && \
    tar -zxvf go1.17.linux-amd64.tar.gz -C /usr/local/

RUN chown -R root:root /usr/local/go

ENV PATH /usr/local/go/bin:$PATH

ENV GOLANG_VERSION 1.17.11


WORKDIR /usr/src/app

RUN pip install awpy

RUN go version

COPY . /usr/src/app/

CMD [ "python3", "parser/parser.py" ]